/*Меняем заголовок h1*/
//document.querySelector('h1').textContent = 'newTitle';
/*Добавляем новый элемент в список*/
//document.querySelector('.js-my-list').innerHTML = document.querySelector('.js-my-list').innerHTML + "<li>newItem1</li>";
/*Изменяем текст для class="item"*/
//document.querySelector('.item').innerHTML = 'newItem2'
/*слушатель события */
//document.addEventListener('DOM loaded',() => console.log(document.querySelector('h1').textContent));
/*стрелочная функция вызываемая кликом по кнопке */
//const changeTitle = () => document.querySelector('h1').textContent = 'Click';

/*
* Задание:
* 1. Реализуем приложение Список дел с функциональностью как на практике. (Возможность добавить новый элемент в список из инпута по нажатию кнопки "Добавить");
* 2. Реализовать удаление элемента по нажатию крестика;
* 3. Реализовать возможность редактирования элемента. 
 - При нажатии на текст элемента он отображается в инпуте, название кнопки меняется на "Обновить". 
 - После изменения названия и нажатия на "Обновить" текст элемента изменяется в списке, инпут сбрасывается, кнопка переименовывается обратно в "Добавить". 
 - Предусмотреть возможность отменить редактирование (крестик в конце инпута), при нажатии на который редактирование прекращается: инпут очищается, кнопка вновь имеет заголовок "Добавить". 
 - Редактируемый элемент подсвечивается как активный.
* 4. При нажатии на галочку текст итема становится зачеркнутым, при отжатии снова обычным.
*/

/* 1. Возможность добавить новый элемент в список из инпута по нажатию кнопки "Добавить");*/
document.querySelector('.panel').addEventListener(
    'click',
    event => {
    /* если цель произошедшего события в имени класса не содержит js-add-item, то мы всплываем*/
      if (!event.target.classList.contains('js-add-item')) {
        return;
      };
      const mode = event.target.getAttribute('data-mode'); // проверка режима работы кнопки Добавить или Обновить
      // если режим Добавить
      if (mode == 'add') {
        // забираем содержимое текстового поля
        const inputText = document.querySelector('.js-input').value;

        // создаем элемент типа checkbox и добавляем ему класс form-check-input
        const input = document.createElement('input');
        input.type = 'checkbox';
        input.classList.add('form-check-input');
        
        // создаем элемент типа span и добавляем текстовый контент
        const spanTxt = document.createElement('span');
        spanTxt.classList.add('text');
        spanTxt.textContent = inputText;

        // создаем элемент типа span и добавляем текстовый контент
        const spanRemove = document.createElement('span');
        spanRemove.classList.add('remove');
        spanRemove.textContent = 'x';

        // создаем счетчик id Для элементов списка
        const id = document.querySelector('.js-list').children.length + 1;

        // создаем структуру из созданных элементов
        const li = document.createElement('li');
        li.setAttribute('id', 'item-' + id);
        li.append(input);
        li.append(spanTxt);
        li.append(spanRemove);

        //console.log(li); // просмотр в консоли структуры
        document.querySelector('.js-list').append(li); // добавляем в ul элемент списка
        document.querySelector('.js-input').value = ''; // очищаем поле ввода

    } else { // иначе режим Обновить
        
        const id = document.querySelector('.js-add-item').getAttribute('data-id-item'); // считываем id пункта списка с атрибута кнопки, который нужно изменить
        // забираем значение из поля ввода и заменяем текст пункта списка
        document.getElementById(id).children[1].textContent = document.querySelector('.js-input').value;  // меняем текст элемента списка на тот, что забрали с поля ввода
        document.querySelector('.js-add-item').textContent = 'Добавить'; // меняем надпись на кнопке
        document.querySelector('.js-add-item').setAttribute('data-mode', 'add'); // меняем режим работы кнопки на Добавление
        document.querySelector('.js-input').value = ''; // очищаем поле ввода
        document.getElementById(id).classList.remove('edit'); // удаляем класс edit с пункта списка с заданым id
    }

    }
);

/* 2. Реализовать удаление элемента по нажатию крестика;*/
document.querySelector('.js-list').addEventListener(
    'click',
    event => {
    /* если цель произошедшего события в имени класса не содержит remove, то мы всплываем*/
      if (!event.target.classList.contains('remove')) {
        return;
      };
      /*если не всплыли, то ищем у цели ближайший элемент li и удаляем его */
      event.target.closest('li').remove();
    }
);

/* 3. Реализовать возможность редактирования элемента. 
 - При нажатии на текст элемента он отображается в инпуте, название кнопки меняется на "Обновить". 
 - После изменения названия и нажатия на "Обновить" текст элемента изменяется в списке, инпут сбрасывается, кнопка переименовывается обратно в "Добавить". 
 - Предусмотреть возможность отменить редактирование (крестик в конце инпута), при нажатии на который редактирование прекращается: инпут очищается, кнопка вновь имеет заголовок "Добавить". 
 - Редактируемый элемент подсвечивается как активный*/
 document.querySelector('.js-list').addEventListener(
    'click',
    event => {
    /* если цель произошедшего события в имени класса не содержит text, то мы всплываем*/
      if (!event.target.classList.contains('text')) {
        return;
      };
      document.querySelector('.js-input').value = event.target.textContent; // переносим значение пункта списка в поле для ввода
      document.querySelector('.js-add-item').textContent = 'Обновить'; // меняем надпись на кнопке на Обновить
      document.querySelector('.js-add-item').setAttribute('data-mode', 'refresh'); // переключаем режим работы кнопки на Обновить
      document.querySelector('.js-input').setAttribute('type', 'search'); // переключаем атрибут поля ввода с input на search, чтобы появился крестик
      const idItem = event.target.parentElement.getAttribute('id'); // забираем id элемента списка, по которому был клик
      document.querySelector('.js-add-item').setAttribute('data-id-item', idItem); // устанавливаем кнопке атрибут с id пункта, который был кликнут
      event.target.closest('li').classList.toggle('edit'); // переключаем класс на edit для пункта списка
    }
)


document.querySelector('.js-cancel-update-mode').addEventListener(
  'click',
  event => {
    /* проверяем атрибут type, если он не search, то всплываем*/
    const typeSearch = document.querySelector('.js-input').getAttribute('type');
    if (typeSearch == 'search') {
      document.querySelector('.js-input').value = ''; // очищаем поле ввода
      document.querySelector('.js-input').setAttribute('type', 'input'); // переключаем атрибут поля ввода с search  на input после удаления текста
      document.querySelector('.js-add-item').textContent = 'Добавить'; // меняем надпись на кнопке
      document.querySelector('.js-add-item').setAttribute('data-mode', 'add'); // меняем режим работы кнопки на Добавление
      document.querySelector('.edit').classList.remove('edit'); // удаляем класс edit с пункта с этим классом        
    };    
  }
)

 /* 4. При нажатии на галочку текст итема становится зачеркнутым, при отжатии снова обычным.*/

 document.querySelector('.js-list').addEventListener(
    'click',
    event => {
    /* если цель произошедшего события в имени класса не содержит form-check-input, то мы всплываем*/
      if (!event.target.classList.contains('form-check-input')) {
        return;
      };
      /*если не всплыли, то ищем у цели ближайший элемент li и переключаем его класс на done */
      event.target.closest('li').classList.toggle('done');
    }
)